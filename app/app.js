(function() {
    'use strict';

    var opentele = angular.module('opentele', [
        'ngRoute',

        'opentele.config',
        'opentele.controllers',
        'opentele.directives',
        'opentele.exceptionHandler',
        'opentele.listeners',
        'opentele.services',
        'opentele.translations',
        'templates-dist',
        'opentele.plugins',
        'ngIdle',
        'opentele-commons.nativeServices'
    ]);

    opentele.config(function($routeProvider, IdleProvider, appConfig) {
        $routeProvider
            .otherwise({
                redirectTo: '/login'
            });

        IdleProvider.idle(appConfig.idleTimeoutInSeconds);
        IdleProvider.timeout(appConfig.idleWarningCountdownInSeconds);
        IdleProvider.keepalive(false);
    });

    opentele.controller('AppCtrl', function($scope, $window, $translate,
                                            languages, changeLocale) {

        $scope.$watch(function() {
            return $window.navigator.userLanguage;
        }, function (newValue, oldValue) {
            if (newValue !== undefined && newValue !== oldValue) {
                changeLocale(newValue);
            }
        });
    });

    opentele.run(function(nativeService) {
        nativeService.enableMessagesFromNative();
    });

    opentele.run(function($rootScope, $location, $route, $translate,
                          headerService, appContext) {
        var setRouteEvents = function() {
            var openRoutes = [];
            openRoutes.push("");
            angular.forEach($route.routes, function(route, path) {
                if (route.publicAccess) {
                    openRoutes.push(path);
                }
            });
            $rootScope.$on('$locationChangeStart', function(event) {
                if (appContext.currentUser.isValid()) {
                    return;
                }

                var closed = (openRoutes.indexOf($location.path()) === -1);
                if (closed) {
                    event.preventDefault();
                    $location.path('/login');
                }
            });

            $rootScope.$on('$routeChangeSuccess', function(event, current) {
                headerService.setHeader(true);
                headerService.setMainMenu(true);

                if (current.$$route !== undefined) {
                    if (typeof($rootScope.sharedModel) === 'undefined') {
                        $rootScope.sharedModel = {};
                    }
                    $rootScope.sharedModel.title = current.$$route.title;
                    $translate(current.$$route.title).then(function(translation) {
                        $rootScope.sharedModel.title = translation;
                    });
                }
            });
        };

        setRouteEvents();
    });

    opentele.run(function($rootScope, $timeout, $location, $log, Idle) {

        var initializeUserIdleEvents = function() {
            $rootScope.$on('IdleTimeout', function() {
                $timeout(function(){ // will force an apply to be called which is needed for the location to change...
                    $log.info("User idle and logged out");
                    $location.path('/login');
                });
            });
        };

        var initializeAppLifeCycleEvents = function() {
            $rootScope.lifeCycleEvents = {
                appCreated: function() {
                    $log.info("App created");
                },
                appResumed: function () {
                    $log.info("App resumed");
                    Idle.interrupt();
                }
            };
        };

        initializeUserIdleEvents();
        initializeAppLifeCycleEvents();
    });
}());
