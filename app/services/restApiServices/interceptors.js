(function () {
    'use strict';

    var interceptors = angular.module('opentele.restApiServices.interceptors', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.restApiServices.httpNotifications'
    ]);

    interceptors.config(function ($httpProvider, httpNotificationsProvider) {

        $httpProvider.interceptors.push(function ($q, $location, appContext) {

            var responseError = function (rejection) {
                var status = rejection.status;
                console.log("HTTP request failed with status code: " + status);
                
                if (rejection.config.errorPassThrough === true) {
                    return $q.reject(rejection);
                }

                if (rejection.config.allowTimeout === true && status === 0) {
                    return $q.reject(rejection);
                }

                if (status === 401) {
                    appContext.requestParams.set('authenticationError', 'LOGGED_OUT');
                    $location.path('/login');
                    return $q.reject(rejection);
                }
                $location.path('/error');
                return $q.reject(rejection);
            };

            return {
                responseError: responseError
            };
        });

        $httpProvider.interceptors.push(function ($q) {

            var request = function (request) {
                if (request.silentRequest === true) {
                    return request;
                } else {
                    httpNotificationsProvider.fireRequestStarted(request);
                    return request;
                }
            };

            var response = function (response) {
                httpNotificationsProvider.fireRequestEnded(response);
                return response;
            };

            var responseError = function (rejection) {
                httpNotificationsProvider.fireRequestEnded(rejection);
                return $q.reject(rejection);
            };

            return {
                request: request,
                response: response,
                responseError: responseError
            };
        });

    });
}());
