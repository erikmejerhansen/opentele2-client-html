(function() {
    'use strict';

    var medicineList = angular.module('opentele.medicineList', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.translations',
        'opentele.medicineList.restApiServices',
        'pdf']);

	medicineList.config(function($routeProvider) {
		$routeProvider
			.when('/medicine_list', {
				title: 'SHOW_MEDICINELIST',
				templateUrl: 'plugins/medicineList/medicineList.html'
			});
	});

	medicineList.config(function($translateProvider) {
		$translateProvider.translations('en-US', enUsTranslations);
		$translateProvider.translations('da-DK', daDkTranslations);
	});

	medicineList.run(function(pluginRegistry, medicineListSummary, $translate) {
		pluginRegistry.registerPlugin({
			menuItem: {
				name: $translate.instant('MENU_MEDICINELIST'),
				url: '#/medicine_list',
				enabled: function(user) {
					return user.hasOwnProperty('links') && user.links.hasOwnProperty('medicineListSummary');
				},
                added: function(user, menuItem) {
                    medicineListSummary.get(user, function(summary) {
                        menuItem.name = $translate.instant('MENU_MEDICINELIST');
                        if (summary.isNew === true) {
                            menuItem.name = menuItem.name + " (" + $translate.instant('MENU_MEDICINELIST_NEW') + ")";
                        }
                    });
                }
			}
		});
	});

    medicineList.controller('MedicineListCtrl', function($scope, $timeout, $window, $templateCache, appContext, pdfDelegate, medicineListSummary) {
        var user = appContext.currentUser.get();

        $scope.$on('backClick', function() {
            $window.history.back();
        });

        var pdfHandle = function() {
            return pdfDelegate.$getByHandle('medicine-list-pdf-container');
        };

        var showMedicineList = function() {
            $scope.model.headers = {
                'authorization': medicineListSummary.authorizationHeaderValue()
            };

            medicineListSummary.get(user, function(summary) {
                $scope.model.medicineList = summary.links.medicineList;
                $scope.model.pdfViewerHtml = $templateCache.get("plugins/medicineList/pdfViewer.html");
            });
        };

        $scope.zoomIn = function() {
            $scope.model.disableZoomIn = true;
            var handle = pdfHandle();
            handle.zoomIn();

            allowForRedraw('disableZoomIn');
        };

        $scope.zoomOut = function() {
            $scope.model.disableZoomOut = true;
            var handle = pdfHandle();
            handle.zoomOut();

            allowForRedraw('disableZoomOut');
        };

        $scope.prev = function() {
            $scope.model.disablePrev = true;
            var handle = pdfHandle();
            handle.prev();

            allowForRedraw('disablePrev');
        };

        $scope.next = function() {
            $scope.model.disableNext = true;
            var handle = pdfHandle();
            handle.next();

            allowForRedraw('disableNext');
        };

        var allowForRedraw = function(disabled) {
            $timeout(function() {
                $scope.model[disabled] = false;
            }, 1250);
        };

        $scope.model = {};
        $scope.model.disableZoomIn = false;
        $scope.model.disableZoomOut = false;
        $scope.model.disablePrev = false;
        $scope.model.disableNext = false;
        $scope.model.pdfViewerHtml = "";

        showMedicineList();
    });

	var enUsTranslations = {
		"SHOW_MEDICINELIST": "Medicine list",
		"MENU_MEDICINELIST": "Medicine list",
        "MENU_MEDICINELIST_NEW": "New",
        "PDF_PREV": "Prev",
        "PDF_NEXT": "Next",
        "PDF_ZOOMIN": "Zoom In",
        "PDF_ZOOMOUT": "Zoom Out"
	};

	var daDkTranslations = {
		"SHOW_MEDICINELIST": "Medicinoversigt",
		"MENU_MEDICINELIST": "Medicinoversigt",
        "MENU_MEDICINELIST_NEW": "Ny",
        "PDF_PREV": "Forrige",
        "PDF_NEXT": "Næste",
        "PDF_ZOOMIN": "Zoom Ind",
        "PDF_ZOOMOUT": "Zoom Ud"
	};
}());
