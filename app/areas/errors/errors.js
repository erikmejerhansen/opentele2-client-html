(function() {
    'use strict';

    var errors = angular.module('opentele.controllers.errors', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.restApiServices',
        'opentele.config',
        'opentele.exceptionHandler'
    ]);

    errors.config(function($routeProvider) {
        $routeProvider.when('/error', {
            title: 'ERROR_TITLE',
            templateUrl: 'areas/errors/errors.html',
            publicAccess: true
        });
    });

    errors.controller('ErrorsCtrl', function($scope, $location, headerService,
                                             appConfig, appContext,
                                             logReportingService, errorCodes) {

        var determineErrorMessage = function(errorCode) {

            var errorDict = {};
            errorDict[errorCodes.UNKNOWN_ERROR] = 'OPENTELE_DOWN_TEXT';
            errorDict[errorCodes.INVALID_QUESTIONNAIRE] = 'OPENTELE_INVALID_QUESTIONNAIRE';
            errorDict[errorCodes.NO_NATIVE_LAYER] = 'OPENTELE_NO_NATIVE_LAYER';

            if (errorDict.hasOwnProperty(errorCode)) {
                return errorDict[errorCode];
            } else {
                return 'OPENTELE_DOWN_TEXT';
            }
        };

        $scope.leaveErrorPage = function() {
            $location.path('/menu');
        };

        headerService.setHeader(false);
        $scope.model = {};
        $scope.model.description = 'OPENTELE_DOWN_TEXT';
        $scope.model.okButtonText = 'OK';
        if (appContext.requestParams.containsKey('exception')) {
            var exception = appContext.requestParams.getAndClear('exception');
            $scope.model.description = determineErrorMessage(exception.code);
            if (appConfig.loggingEnabled) {
                logReportingService.log(exception, function() {
                    console.log('Successfully logged exception.');
                }, function() {
                    console.log('Failed to log exception.');
                });
            }
        }

    });
}());
