(function() {
    'use strict';

    var joinConference = angular.module('opentele.controllers.joinConference', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.listeners',
        'opentele.translations',
        'opentele-commons.nativeServices'
    ]);

    joinConference.config(function($routeProvider) {
        $routeProvider
            .when('/joinConference', {
                title: 'CONFERENCE_TITLE',
                templateUrl: 'areas/conference/joinConference.html'
            });
    });

    joinConference.config(function($translateProvider) {
        $translateProvider.translations('en-US', enUsTranslations);
        $translateProvider.translations('da-DK', daDkTranslations);
    });

    joinConference.controller('JoinConferenceCtrl', function($scope, appContext,
                                                             headerService, nativeService,
                                                             videoListener) {

        headerService.setHeader(false);

        var videoConference = appContext.requestParams.getAndClear('videoConference');

        var videoConferenceDescription = {
            username: videoConference.username,
            roomKey: videoConference.roomKey,
            serviceUrl: videoConference.serviceUrl
        };

        var acceptCall = function() {
            nativeService.stopNotificationSound();
            $scope.model.message = 'CONFERENCE_JOINING_MESSAGE';
            $scope.model.state = "joining";
            nativeService.joinConference(videoConferenceDescription);
        };

        var model = {
            header: 'CONFERENCE_JOIN_HEADER',
            message: 'CONFERENCE_JOIN_MESSAGE',
            state: undefined
        };

        $scope.model = model;
        $scope.acceptCall = acceptCall;

        nativeService.playNotificationSound();
        videoListener.setup(videoConference, model);
    });

    var enUsTranslations = {
        "CONFERENCE_TITLE": "Video consultation",
        "CONFERENCE_JOIN_HEADER": "It is time for a video consultation",
        "CONFERENCE_JOIN_MESSAGE": "Press the OK button to start the consultation",
        "CONFERENCE_JOIN_BUTTON": "OK",
        "CONFERENCE_JOINING_MESSAGE": "Starting video consultation. Please wait.",
        "CONFERENCE_MEASUREMENT_SENT": "Measurements received",
        "CONFERENCE_MEASUREMENT_SENT_ERROR": "Measurements could not be received"
    };

    var daDkTranslations = {
        "CONFERENCE_TITLE": "Videokonsultation",
        "CONFERENCE_JOIN_HEADER": "Det er tid til en video konsultation",
        "CONFERENCE_JOIN_MESSAGE": "Tryk på OK-knappen for at starte konsultationen",
        "CONFERENCE_JOIN_BUTTON": "OK",
        "CONFERENCE_JOINING_MESSAGE": "Starter video-konsultationen",
        "CONFERENCE_MEASUREMENT_SENT": "Målinger modtaget",
        "CONFERENCE_MEASUREMENT_SENT_ERROR": "Målinger kunne ikke modtages"
    };

}());
