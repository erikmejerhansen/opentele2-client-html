(function() {
    'use strict';

    exports.get = {
        "name": "Blodtryk og puls",
        "id": 9,
        "startNode": "107",
        "endNode": "108",
        "nodes": [
            {
                "EndNode": {
                    "nodeName": "108"
                }
            },
            {
                "BloodPressureDeviceNode": {
                    "deviceId": {
                        "type": "String",
                        "name": "106.BP#DEVICE_ID"
                    },
                    "pulse": {
                        "type": "Integer",
                        "name": "106.BP#PULSE"
                    },
                    "meanArterialPressure": {
                        "type": "Integer",
                        "name": "106.BP#MEAN_ARTERIAL_PRESSURE"
                    },
                    "systolic": {
                        "type": "Integer",
                        "name": "106.BP#SYSTOLIC"
                    },
                    "diastolic": {
                        "type": "Integer",
                        "name": "106.BP#DIASTOLIC"
                    },
                    "helpImage": null,
                    "helpText": null,
                    "text": "Blodtryk",
                    "nextFail": "AN_106_CANCEL",
                    "next": "ANSEV_108_D106",
                    "nodeName": "106"
                }
            },
            {
                "AssignmentNode": {
                    "expression": {
                        "value": true,
                        "type": "Boolean"
                    },
                    "variable": {
                        "type": "Boolean",
                        "name": "106.BP##CANCEL"
                    },
                    "next": "ANSEV_108_F106",
                    "nodeName": "AN_106_CANCEL"
                }
            },
            {
                "AssignmentNode": {
                    "expression": {
                        "value": "GREEN",
                        "type": "String"
                    },
                    "variable": {
                        "type": "String",
                        "name": "106.BP##SEVERITY"
                    },
                    "next": "108",
                    "nodeName": "ANSEV_108_F106"
                }
            },
            {
                "AssignmentNode": {
                    "expression": {
                        "value": "GREEN",
                        "type": "String"
                    },
                    "variable": {
                        "type": "String",
                        "name": "106.BP##SEVERITY"
                    },
                    "next": "108",
                    "nodeName": "ANSEV_108_D106"
                }
            },
            {
                "IONode": {
                    "elements": [
                        {
                            "TextViewElement": {
                                "text": "Blodtryk"
                            }
                        },
                        {
                            "TextViewElement": {
                                "text": "S\u00e6t manchetten p\u00e5 armen og tryk p\u00e5 n\u00e6ste n\u00e5r du er klar"
                            }
                        },
                        {
                            "ButtonElement": {
                                "next": "106",
                                "gravity": "center",
                                "text": "N\u00e6ste"
                            }
                        }
                    ],
                    "nodeName": "107"
                }
            }
        ],
        "output": [
            {
                "type": "Integer",
                "name": "106.BP#MEAN_ARTERIAL_PRESSURE"
            },
            {
                "type": "String",
                "name": "106.BP#DEVICE_ID"
            },
            {
                "type": "Integer",
                "name": "106.BP#DIASTOLIC"
            },
            {
                "type": "Integer",
                "name": "106.BP#SYSTOLIC"
            },
            {
                "type": "Integer",
                "name": "106.BP#PULSE"
            },
            {
                "type": "Boolean",
                "name": "106.BP##CANCEL"
            },
            {
                "type": "String",
                "name": "106.BP##SEVERITY"
            }
        ]
    };

}());
