(function() {
	'use strict';

	exports.get = {
		"name": "Text Knude fejler",
		"id": 997,
		"startNode": "380",
		"endNode": "381",
		"nodes": [
			{
				"IONode": {
					"nodeName": "353",
					"elements": [
						{
							"TextViewElement": {
								"text": "When was your last bowel movement?"
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "353.FIELD",
									"type": "String"
								}
							}
                    },
						{
							"ButtonElement": {
								"text": "Næste",
								"gravity": "center",
								"next": "361"
							}
                    }
                ],
					"next": "361"
				}
        },
			{
				"IONode": {
					"nodeName": "376",
					"elements": [
						{
							"TextViewElement": {
								"text": "Where is this other pain?"
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "376.FIELD",
									"type": "String"
								}
							}
                    },
						{
							"ButtonElement": {
								"text": "Næste",
								"gravity": "center",
								"next": "360"
							}
                    }
                ],
					"next": "360"
				}
        },
			{
				"IONode": {
					"nodeName": "355",
					"elements": [
						{
							"TextViewElement": {
								"text": "Have you eaten in the past 24 hours?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_354_L355",
								"rightText": "Ja",
								"rightNext": "AN_354_R355"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_354_R355",
					"next": "ANSEV_354_R355",
					"variable": {
						"name": "355.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_354_L355",
					"next": "ANSEV_354_L355",
					"variable": {
						"name": "355.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_354_R355",
					"next": "354",
					"variable": {
						"name": "355.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_354_L355",
					"next": "354",
					"variable": {
						"name": "355.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "368",
					"elements": [
						{
							"TextViewElement": {
								"text": "Do you have any leg pain?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_373_L368",
								"rightText": "Ja",
								"rightNext": "AN_369_R368"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_369_R368",
					"next": "ANSEV_369_R368",
					"variable": {
						"name": "368.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_373_L368",
					"next": "ANSEV_373_L368",
					"variable": {
						"name": "368.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_369_R368",
					"next": "369",
					"variable": {
						"name": "368.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_373_L368",
					"next": "373",
					"variable": {
						"name": "368.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "357",
					"elements": [
						{
							"TextViewElement": {
								"text": "Is this shortness of breath new or worse than before?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_356_L357",
								"rightText": "Ja",
								"rightNext": "AN_356_R357"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_356_R357",
					"next": "ANSEV_356_R357",
					"variable": {
						"name": "357.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_356_L357",
					"next": "ANSEV_356_L357",
					"variable": {
						"name": "357.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_356_R357",
					"next": "356",
					"variable": {
						"name": "357.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_356_L357",
					"next": "356",
					"variable": {
						"name": "357.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "363",
					"elements": [
						{
							"TextViewElement": {
								"text": "Are you experiencing pain?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_360_L363",
								"rightText": "Ja",
								"rightNext": "AN_364_R363"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_364_R363",
					"next": "ANSEV_364_R363",
					"variable": {
						"name": "363.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_360_L363",
					"next": "ANSEV_360_L363",
					"variable": {
						"name": "363.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_364_R363",
					"next": "364",
					"variable": {
						"name": "363.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_360_L363",
					"next": "360",
					"variable": {
						"name": "363.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "354",
					"elements": [
						{
							"TextViewElement": {
								"text": "Have you had a bowel movement in the last 48 hours?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_353_L354",
								"rightText": "Ja",
								"rightNext": "AN_361_R354"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_361_R354",
					"next": "ANSEV_361_R354",
					"variable": {
						"name": "354.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_353_L354",
					"next": "ANSEV_353_L354",
					"variable": {
						"name": "354.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_361_R354",
					"next": "361",
					"variable": {
						"name": "354.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_353_L354",
					"next": "353",
					"variable": {
						"name": "354.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "370",
					"elements": [
						{
							"TextViewElement": {
								"text": "Does your leg incision have any drainage?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_373_L370",
								"rightText": "Ja",
								"rightNext": "AN_373_R370"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_373_R370",
					"next": "ANSEV_373_R370",
					"variable": {
						"name": "370.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_373_L370",
					"next": "ANSEV_373_L370",
					"variable": {
						"name": "370.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_373_R370",
					"next": "373",
					"variable": {
						"name": "370.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_373_L370",
					"next": "373",
					"variable": {
						"name": "370.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "362",
					"elements": [
						{
							"TextViewElement": {
								"text": "Do you have decreased energy today?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_365_L362",
								"rightText": "Ja",
								"rightNext": "AN_365_R362"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_365_R362",
					"next": "ANSEV_365_R362",
					"variable": {
						"name": "362.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_365_L362",
					"next": "ANSEV_365_L362",
					"variable": {
						"name": "362.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_365_R362",
					"next": "365",
					"variable": {
						"name": "362.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_365_L362",
					"next": "365",
					"variable": {
						"name": "362.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "372",
					"elements": [
						{
							"TextViewElement": {
								"text": "Please enter the percentage displayed on your pulse oximeter in the space provided below"
							}
                    },
						{
							"TextViewElement": {
								"text": "Iltmætning"
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "372.SAT#SATURATION",
									"type": "Integer"
								}
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Undlad",
								"leftNext": "AN_372_CANCEL",
								"leftSkipValidation": true,
								"rightText": "Næste",
								"rightNext": "ANSEV_374_D372",
								"rightSkipValidation": false
							}
                    }
                ],
					"next": "374"
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_372_CANCEL",
					"next": "ANSEV_374_F372",
					"variable": {
						"name": "372.SAT##CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_374_F372",
					"next": "374",
					"variable": {
						"name": "372.SAT##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_374_D372",
					"next": "374",
					"variable": {
						"name": "372.SAT##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "365",
					"elements": [
						{
							"TextViewElement": {
								"text": "Are you able to do your normal activities?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_381_L365",
								"rightText": "Ja",
								"rightNext": "AN_381_R365"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_381_R365",
					"next": "ANSEV_381_R365",
					"variable": {
						"name": "365.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_381_L365",
					"next": "ANSEV_381_L365",
					"variable": {
						"name": "365.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_381_R365",
					"next": "381",
					"variable": {
						"name": "365.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_381_L365",
					"next": "381",
					"variable": {
						"name": "365.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "359",
					"elements": [
						{
							"TextViewElement": {
								"text": "Is this swelling new or worse than before?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_358_L359",
								"rightText": "Ja",
								"rightNext": "AN_358_R359"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_358_R359",
					"next": "ANSEV_358_R359",
					"variable": {
						"name": "359.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_358_L359",
					"next": "ANSEV_358_L359",
					"variable": {
						"name": "359.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_358_R359",
					"next": "358",
					"variable": {
						"name": "359.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_358_L359",
					"next": "358",
					"variable": {
						"name": "359.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "356",
					"elements": [
						{
							"TextViewElement": {
								"text": "Do you have an appetite?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_355_L356",
								"rightText": "Ja",
								"rightNext": "AN_354_R356"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_354_R356",
					"next": "ANSEV_354_R356",
					"variable": {
						"name": "356.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_355_L356",
					"next": "ANSEV_355_L356",
					"variable": {
						"name": "356.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_354_R356",
					"next": "354",
					"variable": {
						"name": "356.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_355_L356",
					"next": "355",
					"variable": {
						"name": "356.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"SaturationWithoutPulseDeviceNode": {
					"nodeName": "377",
					"next": "ANSEV_374_D377",
					"nextFail": "AN_377_CANCEL",
					"text": "Please use your pulse oximeter as instructed",
					"helpText": null,
					"helpImage": null,
					"saturation": {
						"name": "377.SAT#SATURATION",
						"type": "Integer"
					},
					"deviceId": {
						"name": "377.SAT#DEVICE_ID",
						"type": "String"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_377_CANCEL",
					"next": "ANSEV_372_F377",
					"variable": {
						"name": "377.SAT##CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_372_F377",
					"next": "372",
					"variable": {
						"name": "377.SAT##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_374_D377",
					"next": "374",
					"variable": {
						"name": "377.SAT##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "366",
					"elements": [
						{
							"TextViewElement": {
								"text": "Is the pain in your chest new or worse than before?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_367_L366",
								"rightText": "Ja",
								"rightNext": "AN_367_R366"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_367_R366",
					"next": "ANSEV_367_R366",
					"variable": {
						"name": "366.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_367_L366",
					"next": "ANSEV_367_L366",
					"variable": {
						"name": "366.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_367_R366",
					"next": "367",
					"variable": {
						"name": "366.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_367_L366",
					"next": "367",
					"variable": {
						"name": "366.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "369",
					"elements": [
						{
							"TextViewElement": {
								"text": "Is your leg pain new or worse than before?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_370_L369",
								"rightText": "Ja",
								"rightNext": "AN_370_R369"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_370_R369",
					"next": "ANSEV_370_R369",
					"variable": {
						"name": "369.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_370_L369",
					"next": "ANSEV_370_L369",
					"variable": {
						"name": "369.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_370_R369",
					"next": "370",
					"variable": {
						"name": "369.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_370_L369",
					"next": "370",
					"variable": {
						"name": "369.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "378",
					"elements": [
						{
							"TextViewElement": {
								"text": "Please enter your weight into the space provided below."
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "378.WEIGHT",
									"type": "Float"
								}
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Undlad",
								"leftNext": "AN_378_CANCEL",
								"leftSkipValidation": true,
								"rightText": "Næste",
								"rightNext": "ANSEV_363_D378",
								"rightSkipValidation": false
							}
                    }
                ],
					"next": "363"
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_378_CANCEL",
					"next": "ANSEV_363_F378",
					"variable": {
						"name": "378.WEIGHT#CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_363_F378",
					"next": "363",
					"variable": {
						"name": "378.WEIGHT#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_363_D378",
					"next": "363",
					"variable": {
						"name": "378.WEIGHT#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "375",
					"elements": [
						{
							"TextViewElement": {
								"text": "Please enter your blood pressure and pulse information manually into the space provided below."
							}
                    },
						{
							"TextViewElement": {
								"text": "Systolisk blodtryk"
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "375.BP#SYSTOLIC",
									"type": "Integer"
								}
							}
                    },
						{
							"TextViewElement": {
								"text": "Diastolisk blodtryk"
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "375.BP#DIASTOLIC",
									"type": "Integer"
								}
							}
                    },
						{
							"TextViewElement": {
								"text": "Puls"
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "375.BP#PULSE",
									"type": "Integer"
								}
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Undlad",
								"leftNext": "AN_375_CANCEL",
								"leftSkipValidation": true,
								"rightText": "Næste",
								"rightNext": "ANSEV_377_D375",
								"rightSkipValidation": false
							}
                    }
                ],
					"next": "ANSEV_377_D375"
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_375_CANCEL",
					"next": "ANSEV_377_F375",
					"variable": {
						"name": "375.BP##CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_377_F375",
					"next": "377",
					"variable": {
						"name": "375.BP##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_377_D375",
					"next": "377",
					"variable": {
						"name": "375.BP##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"WeightDeviceNode": {
					"nodeName": "374",
					"next": "ANSEV_363_D374",
					"nextFail": "AN_374_CANCEL",
					"text": "Please use your scale as instructed",
					"weight": {
						"name": "374.WEIGHT",
						"type": "Float"
					},
					"deviceId": {
						"name": "374.WEIGHT#DEVICE_ID",
						"type": "String"
					},
					"helpText": null,
					"helpImage": null
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_374_CANCEL",
					"next": "ANSEV_378_F374",
					"variable": {
						"name": "374.WEIGHT#CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_378_F374",
					"next": "378",
					"variable": {
						"name": "374.WEIGHT#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_363_D374",
					"next": "363",
					"variable": {
						"name": "374.WEIGHT#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "361",
					"elements": [
						{
							"TextViewElement": {
								"text": "Did you sleep well last night?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_362_L361",
								"rightText": "Ja",
								"rightNext": "AN_362_R361"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_362_R361",
					"next": "ANSEV_362_R361",
					"variable": {
						"name": "361.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_362_L361",
					"next": "ANSEV_362_L361",
					"variable": {
						"name": "361.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_362_R361",
					"next": "362",
					"variable": {
						"name": "361.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_362_L361",
					"next": "362",
					"variable": {
						"name": "361.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "358",
					"elements": [
						{
							"TextViewElement": {
								"text": "Are you experiencing any shortness of breath?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_356_L358",
								"rightText": "Ja",
								"rightNext": "AN_357_R358"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_357_R358",
					"next": "ANSEV_357_R358",
					"variable": {
						"name": "358.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_356_L358",
					"next": "ANSEV_356_L358",
					"variable": {
						"name": "358.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_357_R358",
					"next": "357",
					"variable": {
						"name": "358.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_356_L358",
					"next": "356",
					"variable": {
						"name": "358.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "367",
					"elements": [
						{
							"TextViewElement": {
								"text": "Does your chest incision have any drainage?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_368_L367",
								"rightText": "Ja",
								"rightNext": "AN_368_R367"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_368_R367",
					"next": "ANSEV_368_R367",
					"variable": {
						"name": "367.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_368_L367",
					"next": "ANSEV_368_L367",
					"variable": {
						"name": "367.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_368_R367",
					"next": "368",
					"variable": {
						"name": "367.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_368_L367",
					"next": "368",
					"variable": {
						"name": "367.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"EndNode": {
					"nodeName": "381"
				}
        },
			{
				"IONode": {
					"nodeName": "360",
					"elements": [
						{
							"TextViewElement": {
								"text": "Do you have any leg, ankle or foot swelling?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_358_L360",
								"rightText": "Ja",
								"rightNext": "AN_359_R360"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_359_R360",
					"next": "ANSEV_359_R360",
					"variable": {
						"name": "360.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_358_L360",
					"next": "ANSEV_358_L360",
					"variable": {
						"name": "360.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_359_R360",
					"next": "359",
					"variable": {
						"name": "360.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_358_L360",
					"next": "358",
					"variable": {
						"name": "360.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "380",
					"elements": [
						{
							"TextViewElement": {
								"text": "Please take your temperature and enter below."
							}
                    },
						{
							"EditTextElement": {
								"outputVariable": {
									"name": "380.TEMPERATURE",
									"type": "Float"
								}
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Undlad",
								"leftNext": "AN_380_CANCEL",
								"leftSkipValidation": true,
								"rightText": "Næste",
								"rightNext": "ANSEV_379_D380",
								"rightSkipValidation": false
							}
                    }
                ],
					"next": "379"
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_380_CANCEL",
					"next": "ANSEV_379_F380",
					"variable": {
						"name": "380.TEMPERATURE#CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_379_F380",
					"next": "379",
					"variable": {
						"name": "380.TEMPERATURE#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_379_D380",
					"next": "379",
					"variable": {
						"name": "380.TEMPERATURE#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"BloodPressureDeviceNode": {
					"nodeName": "379",
					"next": "ANSEV_377_D379",
					"nextFail": "AN_379_CANCEL",
					"text": "Please use your Blood Pressure Cuff as instructed below.",
					"helpText": null,
					"helpImage": null,
					"diastolic": {
						"name": "379.BP#DIASTOLIC",
						"type": "Integer"
					},
					"systolic": {
						"name": "379.BP#SYSTOLIC",
						"type": "Integer"
					},
					"meanArterialPressure": {
						"name": "379.BP#MEAN_ARTERIAL_PRESSURE",
						"type": "Integer"
					},
					"pulse": {
						"name": "379.BP#PULSE",
						"type": "Integer"
					},
					"deviceId": {
						"name": "379.BP#DEVICE_ID",
						"type": "String"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_379_CANCEL",
					"next": "ANSEV_375_F379",
					"variable": {
						"name": "379.BP##CANCEL",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_375_F379",
					"next": "375",
					"variable": {
						"name": "379.BP##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_377_D379",
					"next": "377",
					"variable": {
						"name": "379.BP##SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "364",
					"elements": [
						{
							"TextViewElement": {
								"text": "Are you experiencing pain in your chest?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_368_L364",
								"rightText": "Ja",
								"rightNext": "AN_366_R364"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_366_R364",
					"next": "ANSEV_366_R364",
					"variable": {
						"name": "364.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_368_L364",
					"next": "ANSEV_368_L364",
					"variable": {
						"name": "364.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_366_R364",
					"next": "366",
					"variable": {
						"name": "364.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_368_L364",
					"next": "368",
					"variable": {
						"name": "364.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "373",
					"elements": [
						{
							"TextViewElement": {
								"text": "Are you experiencing any other pain?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_360_L373",
								"rightText": "Ja",
								"rightNext": "AN_371_R373"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_371_R373",
					"next": "ANSEV_371_R373",
					"variable": {
						"name": "373.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_360_L373",
					"next": "ANSEV_360_L373",
					"variable": {
						"name": "373.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_371_R373",
					"next": "371",
					"variable": {
						"name": "373.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "YELLOW"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_360_L373",
					"next": "360",
					"variable": {
						"name": "373.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        },
			{
				"IONode": {
					"nodeName": "371",
					"elements": [
						{
							"TextViewElement": {
								"text": "Is this pain new or worse than before?"
							}
                    },
						{
							"TwoButtonElement": {
								"leftText": "Nej",
								"leftNext": "AN_376_L371",
								"rightText": "Ja",
								"rightNext": "AN_376_R371"
							}
                    }
                ]
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_376_R371",
					"next": "ANSEV_376_R371",
					"variable": {
						"name": "371.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": true
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "AN_376_L371",
					"next": "ANSEV_376_L371",
					"variable": {
						"name": "371.FIELD",
						"type": "Boolean"
					},
					"expression": {
						"type": "Boolean",
						"value": false
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_376_R371",
					"next": "376",
					"variable": {
						"name": "371.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "RED"
					}
				}
        },
			{
				"AssignmentNode": {
					"nodeName": "ANSEV_376_L371",
					"next": "376",
					"variable": {
						"name": "371.FIELD#SEVERITY",
						"type": "String"
					},
					"expression": {
						"type": "String",
						"value": "GREEN"
					}
				}
        }
    ],
		"output": [
			{
				"name": "379.BP#MEAN_ARTERIAL_PRESSURE",
				"type": "Integer"
        },
			{
				"name": "365.FIELD",
				"type": "Boolean"
        },
			{
				"name": "380.TEMPERATURE#SEVERITY",
				"type": "String"
        },
			{
				"name": "356.FIELD",
				"type": "Boolean"
        },
			{
				"name": "363.FIELD",
				"type": "Boolean"
        },
			{
				"name": "362.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "359.FIELD",
				"type": "Boolean"
        },
			{
				"name": "359.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "369.FIELD",
				"type": "Boolean"
        },
			{
				"name": "378.WEIGHT#CANCEL",
				"type": "Boolean"
        },
			{
				"name": "354.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "377.SAT##SEVERITY",
				"type": "String"
        },
			{
				"name": "366.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "374.WEIGHT#CANCEL",
				"type": "Boolean"
        },
			{
				"name": "380.TEMPERATURE#CANCEL",
				"type": "Boolean"
        },
			{
				"name": "378.WEIGHT",
				"type": "Float"
        },
			{
				"name": "377.SAT#DEVICE_ID",
				"type": "String"
        },
			{
				"name": "374.WEIGHT#DEVICE_ID",
				"type": "String"
        },
			{
				"name": "372.SAT##CANCEL",
				"type": "Boolean"
        },
			{
				"name": "371.FIELD",
				"type": "Boolean"
        },
			{
				"name": "379.BP#DIASTOLIC",
				"type": "Integer"
        },
			{
				"name": "368.FIELD",
				"type": "Boolean"
        },
			{
				"name": "377.SAT##CANCEL",
				"type": "Boolean"
        },
			{
				"name": "363.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "361.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "375.BP#DIASTOLIC",
				"type": "Integer"
        },
			{
				"name": "370.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "358.FIELD",
				"type": "Boolean"
        },
			{
				"name": "365.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "357.FIELD",
				"type": "Boolean"
        },
			{
				"name": "374.WEIGHT#SEVERITY",
				"type": "String"
        },
			{
				"name": "379.BP#DEVICE_ID",
				"type": "String"
        },
			{
				"name": "371.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "364.FIELD",
				"type": "Boolean"
        },
			{
				"name": "375.BP#SYSTOLIC",
				"type": "Integer"
        },
			{
				"name": "373.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "356.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "366.FIELD",
				"type": "Boolean"
        },
			{
				"name": "360.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "372.SAT#SATURATION",
				"type": "Integer"
        },
			{
				"name": "378.WEIGHT#SEVERITY",
				"type": "String"
        },
			{
				"name": "358.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "370.FIELD",
				"type": "Boolean"
        },
			{
				"name": "355.FIELD",
				"type": "Boolean"
        },
			{
				"name": "357.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "375.BP##CANCEL",
				"type": "Boolean"
        },
			{
				"name": "373.FIELD",
				"type": "Boolean"
        },
			{
				"name": "369.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "361.FIELD",
				"type": "Boolean"
        },
			{
				"name": "362.FIELD",
				"type": "Boolean"
        },
			{
				"name": "364.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "379.BP##SEVERITY",
				"type": "String"
        },
			{
				"name": "379.BP#SYSTOLIC",
				"type": "Integer"
        },
			{
				"name": "372.SAT##SEVERITY",
				"type": "String"
        },
			{
				"name": "374.WEIGHT",
				"type": "Float"
        },
			{
				"name": "367.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "355.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "379.BP#PULSE",
				"type": "Integer"
        },
			{
				"name": "354.FIELD",
				"type": "Boolean"
        },
			{
				"name": "376.FIELD",
				"type": "String"
        },
			{
				"name": "380.TEMPERATURE",
				"type": "Float"
        },
			{
				"name": "360.FIELD",
				"type": "Boolean"
        },
			{
				"name": "377.SAT#SATURATION",
				"type": "Integer"
        },
			{
				"name": "368.FIELD#SEVERITY",
				"type": "String"
        },
			{
				"name": "375.BP#PULSE",
				"type": "Integer"
        },
			{
				"name": "353.FIELD",
				"type": "String"
        },
			{
				"name": "379.BP##CANCEL",
				"type": "Boolean"
        },
			{
				"name": "367.FIELD",
				"type": "Boolean"
        },
			{
				"name": "375.BP##SEVERITY",
				"type": "String"
        }
    ]
	};
}());
