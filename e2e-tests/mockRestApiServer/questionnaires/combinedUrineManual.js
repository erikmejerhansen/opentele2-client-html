(function () {
    'use strict';

    exports.get = {
        "name": "Urine Measurements",
        "id": 777,
        "version": "4.0",
        "startNode": "240",
        "endNode": "242",
        "nodes": [
            {
                "EndNode": {
                    "nodeName": "242"
                }
            },
            {
                "LeukocytesUrineDeviceNode": {
                    "nodeName": "241",
                    "next": "ANSEV_239_D241",
                    "nextFail": "AN_241_CANCEL",
                    "text": "Leukocytter i urin",
                    "helpText": null,
                    "helpImage": null,
                    "leukocytesUrine": {
                        "name": "241.URINE_LEUKOCYTES",
                        "type": "Integer"
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "AN_241_CANCEL",
                    "next": "ANSEV_239_F241",
                    "variable": {
                        "name": "241.URINE_LEUKOCYTES#CANCEL",
                        "type": "Boolean"
                    },
                    "expression": {
                        "type": "Boolean",
                        "value": true
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "ANSEV_239_F241",
                    "next": "239",
                    "variable": {
                        "name": "241.URINE_LEUKOCYTES#SEVERITY",
                        "type": "String"
                    },
                    "expression": {
                        "type": "String",
                        "value": "GREEN"
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "ANSEV_239_D241",
                    "next": "239",
                    "variable": {
                        "name": "241.URINE_LEUKOCYTES#SEVERITY",
                        "type": "String"
                    },
                    "expression": {
                        "type": "String",
                        "value": "GREEN"
                    }
                }
            },
            {
                "NitriteUrineDeviceNode": {
                    "nodeName": "239",
                    "next": "ANSEV_242_D239",
                    "nextFail": "AN_239_CANCEL",
                    "text": "Nitrit i urin",
                    "helpText": null,
                    "helpImage": null,
                    "nitriteUrine": {
                        "name": "239.URINE_NITRITE",
                        "type": "Integer"
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "AN_239_CANCEL",
                    "next": "ANSEV_242_F239",
                    "variable": {
                        "name": "239.URINE_NITRITE#CANCEL",
                        "type": "Boolean"
                    },
                    "expression": {
                        "type": "Boolean",
                        "value": true
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "ANSEV_242_F239",
                    "next": "242",
                    "variable": {
                        "name": "239.URINE_NITRITE#SEVERITY",
                        "type": "String"
                    },
                    "expression": {
                        "type": "String",
                        "value": "GREEN"
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "ANSEV_242_D239",
                    "next": "242",
                    "variable": {
                        "name": "239.URINE_NITRITE#SEVERITY",
                        "type": "String"
                    },
                    "expression": {
                        "type": "String",
                        "value": "GREEN"
                    }
                }
            },
            {
                "BloodUrineDeviceNode": {
                    "nodeName": "240",
                    "next": "ANSEV_241_D240",
                    "nextFail": "AN_240_CANCEL",
                    "text": "Blod i urin",
                    "helpText": null,
                    "helpImage": null,
                    "bloodUrine": {
                        "name": "240.URINE_BLOOD",
                        "type": "Integer"
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "AN_240_CANCEL",
                    "next": "ANSEV_241_F240",
                    "variable": {
                        "name": "240.URINE_BLOOD#CANCEL",
                        "type": "Boolean"
                    },
                    "expression": {
                        "type": "Boolean",
                        "value": true
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "ANSEV_241_F240",
                    "next": "241",
                    "variable": {
                        "name": "240.URINE_BLOOD#SEVERITY",
                        "type": "String"
                    },
                    "expression": {
                        "type": "String",
                        "value": "GREEN"
                    }
                }
            },
            {
                "AssignmentNode": {
                    "nodeName": "ANSEV_241_D240",
                    "next": "241",
                    "variable": {
                        "name": "240.URINE_BLOOD#SEVERITY",
                        "type": "String"
                    },
                    "expression": {
                        "type": "String",
                        "value": "GREEN"
                    }
                }
            }
        ],
        "output": [
            {
                "name": "241.URINE_LEUKOCYTES#CANCEL",
                "type": "Boolean"
            },
            {
                "name": "240.URINE_BLOOD#CANCEL",
                "type": "Boolean"
            },
            {
                "name": "241.URINE_LEUKOCYTES#SEVERITY",
                "type": "String"
            },
            {
                "name": "240.URINE_BLOOD",
                "type": "Integer"
            },
            {
                "name": "241.URINE_LEUKOCYTES",
                "type": "Integer"
            },
            {
                "name": "239.URINE_NITRITE#CANCEL",
                "type": "Boolean"
            },
            {
                "name": "240.URINE_BLOOD#SEVERITY",
                "type": "String"
            },
            {
                "name": "239.URINE_NITRITE#SEVERITY",
                "type": "String"
            },
            {
                "name": "239.URINE_NITRITE",
                "type": "Integer"
            }
        ]
    };
} ());
    