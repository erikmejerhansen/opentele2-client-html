(function() {
    'use strict';

    var SaturationDeviceNodePage = function() {
        this.heading = element(by.binding('nodeModel.heading'));
        this.info = element(by.binding('nodeModel.info'));
        this.error = element(by.binding('nodeModel.error'));
        this.saturation = element(by.model('nodeModel.saturation'));
        this.pulse = element(by.model('nodeModel.pulse'));
    };

    module.exports = new SaturationDeviceNodePage();

}());
