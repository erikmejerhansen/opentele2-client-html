(function() {
    'use strict';

    describe('walk through and fill out questionnaire containing lung monitor device node', function() {
        var loginPage = require('../../login/loginPage.js');
        var menuPage = require('../../menu/menuPage.js');
        var measurementsPage = require('../performMeasurementsPage.js');
        var lungMonitorDeviceNodePage = require('./lungMonitorDeviceNodePage.js');

        beforeEach(function () {
            loginPage.get();
            loginPage.doLogin('nancyann', 'abcd1234');
            menuPage.toMeasurements();
            measurementsPage.toQuestionnaire("Lungefunktion", "0.1");
        });

        it('should see all info texts and fill out form', function(done) {
            expect(lungMonitorDeviceNodePage.heading.getText()).toMatch(/Måling af lungefunktion/);
            expect(lungMonitorDeviceNodePage.info.getText()).toMatch(/Connecting/);

            var infoDeferred = protractor.promise.defer();
            var fev1Deferred = protractor.promise.defer();
            setTimeout(function() {
                lungMonitorDeviceNodePage.info.getText().then(
                    function(text) {
                        infoDeferred.fulfill(text);
                    });
                lungMonitorDeviceNodePage.fev1.getAttribute('value').then(
                    function(value) {
                        fev1Deferred.fulfill(value);
                        done();
                    });
            }, 3500);
            expect(fev1Deferred).toEqual('3.5');
            expect(infoDeferred).toMatch(/Connected/);

        });
    });

}());
