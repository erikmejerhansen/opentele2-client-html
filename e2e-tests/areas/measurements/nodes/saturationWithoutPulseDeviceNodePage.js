(function() {
    'use strict';

    var SaturationWithoutPulseDeviceNodePage = function() {
        this.heading = element(by.binding('nodeModel.heading'));
        this.info = element(by.binding('nodeModel.info'));
        this.error = element(by.binding('nodeModel.error'));
        this.saturation = element(by.model('nodeModel.saturation'));
    };

    module.exports = new SaturationWithoutPulseDeviceNodePage();

}());
