(function() {
    'use strict';

    var LungMonitorDeviceNodePage = function() {
        this.heading = element(by.binding('nodeModel.heading'));
        this.info = element(by.binding('nodeModel.info'));
        this.error = element(by.binding('nodeModel.error'));
        this.fev1 = element(by.model('nodeModel.fev1'));
    };

    module.exports = new LungMonitorDeviceNodePage();

}());
