(function () {
    'use strict';

    describe('opentele.controllers.joinConference', function () {
        var controller, scope, appContext, nativeService, videoListener, passedArguments;

        beforeEach(module('opentele.stateServices'));
        beforeEach(module('opentele.controllers.joinConference'));
        beforeEach(module('opentele.translations'));

        beforeEach(module(function ($provide) {

            var nativeService = {
                joinConference: function(videoConferenceDescription) {},
                playNotificationSound: function() {},
                stopNotificationSound: function() {}
            };
            $provide.value('nativeService', nativeService);

            var videoListener = {
                setup: function(url, model) {
                    return function(event) {};
                }
            };
            $provide.value('videoListener', videoListener);
        }));

        beforeEach(inject(function ($rootScope, $controller,
                                    _appContext_, _nativeService_, _videoListener_) {
            scope = $rootScope.$new();
            controller = $controller;
            appContext = _appContext_;
            nativeService = _nativeService_;
            videoListener = _videoListener_;

            appContext.requestParams.set('videoConference', {
                username: "NancyAnn",
                roomKey: "237",
                serviceUrl: "http://localhost/serviceUrl"
            });
        }));

        var runController = function() {
            controller('JoinConferenceCtrl', {
                '$scope': scope,
                'appContext': appContext,
                'nativeService': nativeService,
                'videoListener': videoListener
            });
        };

        it('should be defined', function() {
            expect(controller).toBeDefined();
        });

        it('should set scope model correctly', function() {
            spyOn(videoListener, 'setup');

            runController();

            expect(videoListener.setup).toHaveBeenCalled();

            expect(scope.acceptCall).toBeDefined();
            expect(scope.model.header).toEqual('CONFERENCE_JOIN_HEADER');
            expect(scope.model.message).toEqual('CONFERENCE_JOIN_MESSAGE');
            expect(scope.model.state).toEqual(undefined);
        });

        it('should call join conference when accepting a call', function() {
            spyOn(nativeService, 'playNotificationSound');
            spyOn(nativeService, 'stopNotificationSound');
            spyOn(nativeService, 'joinConference');

            runController();

            expect(nativeService.playNotificationSound).toHaveBeenCalled();

            scope.acceptCall();

            expect(nativeService.stopNotificationSound).toHaveBeenCalled();
            expect(scope.model.message).toEqual('CONFERENCE_JOINING_MESSAGE');
            expect(scope.model.state).toEqual('joining');
            expect(nativeService.joinConference).toHaveBeenCalledWith({
                username: "NancyAnn",
                roomKey: "237",
                serviceUrl: "http://localhost/serviceUrl"
            });
        });

    });
}());
