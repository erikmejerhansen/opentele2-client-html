(function () {
    'use strict';

    describe('opentele.controllers.linksCategories', function () {
        var controller, scope, location, appContext, linksCategories, restServiceResult;

        beforeEach(module('opentele.controllers.linksCategories'));

        beforeEach(module(function ($provide) {

            restServiceResult = {
                "categories": [
                    {
                        "name": "Information",
                        "links": {
                            "linkCategory": "http://localhost/patient/links_categories/1"
                        }
                    },
                    {
                        "name": "Video instructions",
                        "links": {
                            "linkCategory": "http://localhost/patient/links_categories/2"
                        }
                    }
                ],
                "links": {
                    "self": "http://localhost/patient/links_categories"
                }
            };

            linksCategories = {
                listFor: function (user, onSuccess) {
                    onSuccess(restServiceResult);
                }
            };

            $provide.value('linksCategories', linksCategories);
        }));

        beforeEach(inject(function ($rootScope, $location, $controller, _appContext_) {
            scope = $rootScope.$new();
            location = $location;
            controller = $controller;
            appContext = _appContext_;

            appContext.currentUser.set({
                firstName: "first name",
                lastName: "last name",
                links: {
                    linksCategories: "http://localhost/patient/links_categories"
                }});
        }));

        var runController = function() {
            controller('LinksCategoriesCtrl', {
                '$scope': scope,
                '$location': location,
                'appContext': appContext,
                'linksCategories': linksCategories
            });
        };

        it('should be defined', function() {
            expect(controller).toBeDefined();
        });

        it('should get all links categories for user', function() {
            runController();
            expect(scope.model).toEqual(restServiceResult);
        });

        it('should open links category when clicked', function() {
            runController();

            var linkIndex = 1;
            scope.showLinksCategory(linkIndex);
            expect(location.path()).toEqual('/category_links');
            expect(appContext.requestParams.getAndClear('selectedLinksCategory')).toEqual(restServiceResult.categories[linkIndex]);
        });

        it('should automatically redirect to links category if list only contains one', function () {
            restServiceResult = {
                "categories": [
                    {
                        "name": "Information",
                        "links": {
                            "linkCategory": "http://localhost/patient/links_categories/1"
                        }
                    }
                ],
                "links": {
                    "self": "http://localhost/patient/links_categories"
                }
            };
            runController();

            expect(location.path()).toEqual('/category_links');
        });
    });
}());
