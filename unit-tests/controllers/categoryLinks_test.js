(function () {
    'use strict';

    describe('opentele.controllers.categoryLinks', function () {
        var controller, scope, location, window, appContext, linksCategories, restServiceResult, nativeService;

        beforeEach(module('opentele.controllers.categoryLinks'));

        beforeEach(module(function ($provide) {

            restServiceResult = {
                "name": "Information",
                "categoryLinks": [
                    {
                        "title": "OpenTele user manual",
                        "url": "http://opentele.silverbullet.dk/applications"
                    },
                    {
                        "title": "Saturation measurement guide",
                        "url": "http://opentele.silverbullet.dk/architecture"
                    }
                ],
                "links": {
                    "self": "http://localhost/patient/links_categories/1"
                }
            };

            linksCategories = {
                get: function (linksCategoryRef, onSuccess) {
                    onSuccess(restServiceResult);
                }
            };

            window = {};
            
            nativeService = {
                openUrl: function(url) {
                    
                }
            };

            $provide.value('$window', window);
            $provide.value('linksCategories', linksCategories);
            $provide.value('nativeService', nativeService);
        }));

        beforeEach(inject(function ($rootScope, $location, $controller, _appContext_) {
            scope = $rootScope.$new();
            location = $location;
            controller = $controller;
            appContext = _appContext_;

            appContext.requestParams.set('selectedLinksCategory', {
                "name": "Information",
                "links": {
                    "linkCategory": "http://localhost/patient/links_categories/1"
                }
            });
        }));

        var runController = function() {
            controller('CategoryLinksCtrl', {
                '$scope': scope,
                '$window': window,
                '$location': location,
                'appContext': appContext,
                'linksCategories': linksCategories,
                'nativeService': nativeService
            });
        };

        it('should be defined', function() {
            expect(controller).toBeDefined();
        });

        it('should get all links for user', function() {
            runController();
            expect(scope.model.name).toEqual(restServiceResult.name);
            expect(scope.model.categoryLinks).toEqual(restServiceResult.categoryLinks);
        });

        it('should open link popup when clicked', function() {
            runController();

            var linkIndex = 0;
            scope.showPopup(linkIndex);
            expect(scope.model.showPopup).toEqual(true);
            expect(scope.model.currentLink).toEqual(scope.model.categoryLinks[linkIndex].url);
        });

        it('should reset current link and flag when clicking back in popup menu', function() {
            runController();

            var linkIndex = 0;
            scope.showPopup(linkIndex);
            scope.hidePopup();
            expect(scope.model.showPopup).toEqual(false);
            expect(scope.model.currentLink).toEqual(undefined);
        });

        it('should change location when clicking continue in popup menu', function() {
            spyOn(nativeService, 'openUrl');
            
            runController();

            var linkIndex = 0;
            scope.showPopup(linkIndex);
            var selectedUrl = scope.model.currentLink;
            
            scope.showLink();
            
            expect(selectedUrl).toEqual(restServiceResult.categoryLinks[linkIndex].url);
            expect(nativeService.openUrl).toHaveBeenCalledWith(selectedUrl);
            expect(scope.model.currentLink).toBeUndefined(); // cleared
        });

    });
}());
