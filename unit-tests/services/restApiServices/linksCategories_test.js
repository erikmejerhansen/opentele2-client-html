(function() {
    'use strict';

    describe('opentele.restApiServices.linksCategories service', function() {

        var httpBackend, httpProvider;
        var linksCategories;

        beforeEach(module('opentele.restApiServices.linksCategories'));

        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        beforeEach(inject(function($http) {
            httpProvider = $http;
        }));

        beforeEach(inject(function(_linksCategories_, $httpBackend) {
            linksCategories = _linksCategories_;
            httpBackend = $httpBackend;
        }));

        describe('list all links categories for user', function() {

            it('should not invoke callback when status is 401', function() {
                var successCallbackInvoked = false;

                var user = {
                    links: {
                        linksCategories: 'http://localhost/patient/links_categories'
                    }
                };
                httpBackend.whenGET(user.links.linksCategories).respond(401);

                linksCategories.listFor(user,
                    function() {
                        successCallbackInvoked = true;
                    });

                httpBackend.flush();
                expect(successCallbackInvoked).toBe(false);
            });

            it('should throw exception when user has no link to linksCategories', function() {
                expect(function() {
                    linksCategories.listFor({});
                }).toThrow();

                expect(function() {
                    linksCategories.listFor({links: {}});
                }).toThrow();
            });

            it('should invoke success callback when response is valid', function() {
                var successCallbackInvoked = false;

                var user = {
                    links: {
                        linksCategories: 'http://localhost/patient/links_categories'
                    }
                };
                httpBackend.whenGET(user.links.linksCategories).respond({});

                linksCategories.listFor(user,
                    function() {
                        successCallbackInvoked = true;
                    });

                httpBackend.flush();
                expect(successCallbackInvoked).toEqual(true);
            });

            it('should transform response to client object', function() {
                var user = {
                    links: {
                        linksCategories: 'http://localhost/patient/links_categories'
                    }
                };

                httpBackend.whenGET(user.links.linksCategories).respond({
                    "categories": [
                        {
                            "name": "Information",
                            "links": {
                                "linkCategory": "http://localhost/patient/links_categories/1"
                            }
                        },
                        {
                            "name": "Video instructions",
                            "links": {
                                "linkCategory": "http://localhost/patient/links_categories/2"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://localhost/patient/links_categories"
                    }
                });

                var data = {};
                linksCategories.listFor(user, function(response) {
                    data = response;
                });

                httpBackend.flush();

                var informationIndex = 0;
                var videoInstructionsIndex = 1;
                expect(data.categories.length).toEqual(2);
                expect(data.categories[informationIndex].name).toEqual("Information");
                expect(data.categories[informationIndex].links.linkCategory).toEqual("http://localhost/patient/links_categories/1");
                expect(data.categories[videoInstructionsIndex].name).toEqual("Video instructions");
                expect(data.categories[videoInstructionsIndex].links.linkCategory).toEqual("http://localhost/patient/links_categories/2");
            });

        });

        describe('get specific category links', function() {

            var testCategoryLinks = {
                "name": "Information",
                "categoryLinks": [
                    {
                        "title": "OpenTele user manual",
                        "url": "http://opentele.silverbullet.dk/applications"
                    },
                    {
                        "title": "Saturation measurement guide",
                        "url": "http://opentele.silverbullet.dk/architecture"
                    }
                ],
                "links": {
                    "self": "http://localhost/patient/links_categories/1"
                }
            };

            it('should transform response to client object', function() {

                var categoryLinksRef = {
                    "name": "Information",
                    "links": {
                        "linksCategory": "http://localhost/patient/links_categories/1"
                    }
                };

                var getUrl = categoryLinksRef.links.linkCategory;
                httpBackend.whenGET(getUrl).respond(testCategoryLinks);

                var data = {};
                linksCategories.get(categoryLinksRef, function(response) {
                    data = response;
                });
                httpBackend.flush();

                expect(data.name).toEqual("Information");
                expect(data.categoryLinks.length).toEqual(2);

                var userManualIndex = 0;
                expect(data.categoryLinks[userManualIndex].title).toEqual("OpenTele user manual");
                expect(data.categoryLinks[userManualIndex].url).toEqual("http://opentele.silverbullet.dk/applications");

                var measurementGuideIndex = 1;
                expect(data.categoryLinks[measurementGuideIndex].title).toEqual("Saturation measurement guide");
                expect(data.categoryLinks[measurementGuideIndex].url).toEqual("http://opentele.silverbullet.dk/architecture");

            });

            it('should throw exception if links is not defined', function() {
                var wrapperEmpty = function() {
                    linksCategories.get({}, function(response) {});
                };
                var wrapperNoLink = function() {
                    linksCategories.get({links: {}}, function(response) {});
                };
                expect(wrapperEmpty).toThrow();
                expect(wrapperNoLink).toThrow();
            });
        });

    });
}());
